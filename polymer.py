#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sps

global SIZE, RSIZE, types, links

SIZE = 100000
RSIZE = 1000

types = np.full(SIZE, "W", dtype=np.character)
links = np.full((SIZE, 4), -1, dtype=np.int64)
links[::2, 0] = np.arange(1, SIZE, 2)
links[1::2, 0] = np.arange(0, SIZE, 2)


def react(a, b):
    global types
    ww = np.logical_and(types[a] == b"W", types[b] == b"W")
    wx = np.logical_and(types[a] == b"W", types[b] == b"X")
    wz = np.logical_and(types[a] == b"W", types[b] == b"Z")

    xw = np.logical_and(types[a] == b"X", types[b] == b"W")

    zw = np.logical_and(types[a] == b"Z", types[b] == b"W")

    types[a[ww]] = "Z"
    types[b[ww]] = "0"  # Null element
    links[a[ww], 1] = links[b[ww], 0]
    links[links[b[ww], 0], 0] = a[ww]
    links[b[ww], :] = -1

    # print("---WZ---")
    # print(wz)
    types[a[wz]] = "0"
    types[b[wz]] = "X"
    links[b[wz], 2] = links[a[wz], 0]
    links[links[a[wz], 0], 0] = b[wz]
    links[a[wz], :] = -1

    types[b[zw]] = "0"
    types[a[zw]] = "X"
    links[a[zw], 2] = links[b[zw], 0]
    links[links[b[zw], 0], 0] = a[zw]
    links[b[zw], :] = -1

    types[a[wx]] = "0"
    types[b[wx]] = "Y"
    links[b[wx], 3] = links[a[wx], 0]
    links[links[a[wx], 0], 0] = b[wx]
    links[a[wx], :] = -1

    types[b[xw]] = "0"
    types[a[xw]] = "Y"
    links[a[xw], 3] = links[b[xw], 0]
    links[links[b[xw], 0], 0] = a[xw]
    links[b[xw], :] = -1

    return len(
        np.where(
            a[
                np.logical_or(
                    np.logical_or(ww, wx),
                    np.logical_or(
                        np.logical_or(wz, zw),
                        xw))])[0])


def make_graph():
    link_fronts = np.array(list(range(SIZE)) * 4)
    link_backs = np.reshape(links.T, (SIZE*4, ))
    mask = link_backs != -1
    link_fronts = link_fronts[mask]
    link_backs = link_backs[mask]

    graph = sps.csr_matrix((np.ones(link_fronts.size),
                            (link_fronts, link_backs)),
                           shape=(SIZE, SIZE))
    return graph


def get_stats(idx=0):
    d = {}
    d["w"] = len(np.where(types == b"W")[0])
    d["x"] = len(np.where(types == b"X")[0])
    d["y"] = len(np.where(types == b"Y")[0])
    d["z"] = len(np.where(types == b"Z")[0])
    d["total"] = d["w"] + d["x"] + d["y"] + d["z"]

    count, chains = sps.csgraph.connected_components(make_graph(),
                                                     directed=False)
    chains = np.histogram(chains, bins=np.max(chains))[0]
    chains = chains[chains > 1]
    count = chains.size

    d["chains"] = count
    d["mean_nodes"] = np.mean(chains)
    d["median_nodes"] = np.median(chains)
    d["std_nodes"] = np.std(chains)
    d["min_nodes"] = np.min(chains)
    d["max_nodes"] = np.max(chains)
    return d


def plot_sim(d):
    plt.yscale("linear")
    plt.xscale("linear")

    plt.plot(d["reactions"], d["ws"]/d["totals"], label="W")
    plt.plot(d["reactions"], d["xs"]/d["totals"], label="X")
    plt.plot(d["reactions"], d["ys"]/d["totals"], label="Y")
    plt.plot(d["reactions"], d["zs"]/d["totals"], label="Z")
    plt.legend()
    plt.figure()

    plt.plot(d["reactions"], d["chains"], label="chains")
    plt.legend()
    plt.figure()

    plt.plot(d["reactions"], d["means"], label="means")
    plt.plot(d["reactions"], d["medians"], label="medians")
    plt.plot(d["reactions"], d["stds"], label="stds")
    plt.plot(d["reactions"], d["mins"], label="min")
    plt.plot(d["reactions"], d["maxs"], label="max")
    plt.xscale("log")
    plt.yscale("log")
    plt.legend()

    plt.show()


def simulate():
    d = {}
    d["ws"] = [SIZE]
    d["xs"] = [0]
    d["ys"] = [0]
    d["zs"] = [0]
    d["totals"] = [SIZE]
    d["reactions"] = [0]
    d["chains"] = [SIZE//2]
    d["means"] = [2]
    d["medians"] = [2]
    d["stds"] = [0]
    d["mins"] = [2]
    d["maxs"] = [2]

    reactions = 0
    idx = 0
    while(d["ws"][-1] != 0):
        # for i in range(1600):
        perm = np.random.permutation(SIZE)[:RSIZE]
        perma = perm[:RSIZE//2]
        permb = perm[RSIZE//2:]
        reactions += react(perma, permb)
        if idx % 10 == 0:
            stats = get_stats()
            d["reactions"].append(reactions)
            d["ws"].append(stats["w"])
            d["xs"].append(stats["x"])
            d["ys"].append(stats["y"])
            d["zs"].append(stats["z"])
            d["totals"].append(stats["total"])
            d["chains"].append(stats["chains"])
            d["means"].append(stats["mean_nodes"])
            d["medians"].append(stats["median_nodes"])
            d["stds"].append(stats["std_nodes"])
            d["mins"].append(stats["min_nodes"])
            d["maxs"].append(stats["max_nodes"])
        idx += 1

    d["ws"] = np.array(d["ws"])
    d["xs"] = np.array(d["xs"])
    d["ys"] = np.array(d["ys"])
    d["zs"] = np.array(d["zs"])
    d["totals"] = np.asarray(d["totals"], dtype=np.float64)
    d["chains"] = np.array(d["chains"])
    d["means"] = np.array(d["means"])
    d["medians"] = np.array(d["medians"])
    d["stds"] = np.array(d["stds"])

    return d

d = simulate()
plot_sim(d)

# import cProfile
# cProfile.run("simulate()", sort="tottime")
